ready = ->
  showMoveToTop = false

  $(window).scroll () ->
    if $(window).scrollTop() > 200 && !showMoveToTop
      $('.move_to_top').addClass('visible')
      showMoveToTop = true
      
    if $(window).scrollTop() <= 200 && showMoveToTop
      $('.move_to_top').removeClass('visible')
      showMoveToTop = false
  $('.move_to_top').click () ->
    $('html, body').animate({scrollTop:0}, 'slow')
$(document).ready(ready)
$(document).on('page:load', ready)
